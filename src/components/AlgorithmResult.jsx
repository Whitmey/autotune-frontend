import React, { Component } from 'react';
import MaterialCard from './MaterialCard';
import "../styles/AlgorithmResult.scss";

export default class AlgorithmResult extends Component{

  constructor(props) {
    super(props)
  }

  render() {
    let responseBasals = this.props.response.data.basals;
    let reducedBasals = [];
    for(let i=0; i < responseBasals.length; i=i+2) {
      reducedBasals.push(responseBasals[i]);
    }
    console.log(reducedBasals);
    return(
      <div className="algorithm-result">
        <MaterialCard className="previous-settings">
          <h2>Previous Settings</h2>
          <div className="details">
            <span>
              <label>Insulin Sensitivity Factor (ISF)</label>
              <p>{this.props.response.data.sensitivityFactors.pump}</p>
            </span>
            <span>
              <label>Carb Ratio (CR)</label>
              <p>{this.props.response.data.carbRatios.pump}</p>
            </span>
          </div>
          <div className="basals">
            <h3>Basal Rates</h3>
            <table>
              <tbody>
                {reducedBasals.map((basal, i) =>
                  <tr key={i}>
                    <td>{basal.time}</td>
                    <td>{basal.pump}</td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </MaterialCard>
        <MaterialCard className="autotune-settings">
          <h2>AutoTune Settings</h2>
          <div className="details">
            <span>
              <label>Insulin Sensitivity Factor (ISF)</label>
              <p>{this.props.response.data.sensitivityFactors.autotune}</p>
            </span>
            <span>
              <label>Carb Ratio (CR)</label>
              <p>{this.props.response.data.carbRatios.autotune}</p>
            </span>
          </div>
          <div className="basals">
            <h3>Basal Rates</h3>
            <table>
              <tbody>
                {reducedBasals.map((basal, i) =>
                  <tr key={i}>
                    <td>{basal.time}</td>
                    <td>{basal.autotune}</td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </MaterialCard>
      </div>
    )
  }
}

// {
//     "sensitivityFactors": {
//         "pump": "81.000",
//         "autotune": "76.140"
//     },
//     "carbRatios": {
//         "pump": "7.000",
//         "autotune": "6.564"
//     },
//     "basals": [
//         {
//             "time": "00:00",
//             "pump": "0.600",
//             "autotune": "0.613"
//         },
//         {
//             "time": "00:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "01:00",
//             "pump": "",
//             "autotune": "0.590"
//         },
//         {
//             "time": "01:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "02:00",
//             "pump": "",
//             "autotune": "0.595"
//         },
//         {
//             "time": "02:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "03:00",
//             "pump": "",
//             "autotune": "0.592"
//         },
//         {
//             "time": "03:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "04:00",
//             "pump": "",
//             "autotune": "0.591"
//         },
//         {
//             "time": "04:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "05:00",
//             "pump": "",
//             "autotune": "0.602"
//         },
//         {
//             "time": "05:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "06:00",
//             "pump": "",
//             "autotune": "0.608"
//         },
//         {
//             "time": "06:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "07:00",
//             "pump": "",
//             "autotune": "0.601"
//         },
//         {
//             "time": "07:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "08:00",
//             "pump": "0.850",
//             "autotune": "0.838"
//         },
//         {
//             "time": "08:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "09:00",
//             "pump": "0.700",
//             "autotune": "0.742"
//         },
//         {
//             "time": "09:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "10:00",
//             "pump": "",
//             "autotune": "0.723"
//         },
//         {
//             "time": "10:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "11:00",
//             "pump": "",
//             "autotune": "0.697"
//         },
//         {
//             "time": "11:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "12:00",
//             "pump": "",
//             "autotune": "0.669"
//         },
//         {
//             "time": "12:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "13:00",
//             "pump": "",
//             "autotune": "0.690"
//         },
//         {
//             "time": "13:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "14:00",
//             "pump": "",
//             "autotune": "0.701"
//         },
//         {
//             "time": "14:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "15:00",
//             "pump": "",
//             "autotune": "0.699"
//         },
//         {
//             "time": "15:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "16:00",
//             "pump": "",
//             "autotune": "0.699"
//         },
//         {
//             "time": "16:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "17:00",
//             "pump": "",
//             "autotune": "0.699"
//         },
//         {
//             "time": "17:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "18:00",
//             "pump": "",
//             "autotune": "0.699"
//         },
//         {
//             "time": "18:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "19:00",
//             "pump": "",
//             "autotune": "0.699"
//         },
//         {
//             "time": "19:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "20:00",
//             "pump": "",
//             "autotune": "0.699"
//         },
//         {
//             "time": "20:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "21:00",
//             "pump": "",
//             "autotune": "0.691"
//         },
//         {
//             "time": "21:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "22:00",
//             "pump": "",
//             "autotune": "0.720"
//         },
//         {
//             "time": "22:30",
//             "pump": "",
//             "autotune": ""
//         },
//         {
//             "time": "23:00",
//             "pump": "0.850",
//             "autotune": "0.849"
//         },
//         {
//             "time": "23:30",
//             "pump": "",
//             "autotune": ""
//         }
//     ]
// }
