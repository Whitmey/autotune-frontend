import React, { Component } from 'react';
import NetworkService from '../services/NetworkService.js';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import "../styles/FeedbackDialog.scss";

function getModalStyle() {
  return {
    top: `50%`,
    left: `50%`,
    transform: `translate(-50%, -50%)`,
  };
}

export default class FeedbackDialog extends Component {

  constructor(props) {
    super(props);
    this.NetworkService = new NetworkService();
    this.state = {
      open: false,
      content: ''
    };
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleContentChange = this.handleContentChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleOpen() {
    this.setState({ open: true });
  };

  handleClose() {
    this.setState({ open: false });
  };

  handleContentChange(event) {
    this.setState({
      content: event.target.value
    })
  };

  handleSubmit(event) {
    event.preventDefault();
    this.NetworkService.submitFeedback(this.state)
  };

  render() {
    return (
      <div>
        <a className="open-button" onClick={this.handleOpen}>Feedback</a>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className="dialog-container">
            <Typography variant="h6" id="modal-title">
              Submit Feedback
            </Typography>
            <form onSubmit={this.handleSubmit}>
              <textarea type="text" name="content" value={this.state.content} onChange={this.handleContentChange} />
              <input type="submit" value="Submit" className="common-button" id="submit-button"/>
            </form>
          </div>
        </Modal>
      </div>
    );
  }
}
