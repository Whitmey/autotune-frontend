import React, { Component } from 'react';
import "../styles/MaterialCard.scss";

export default class MaterialCard extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div className={"material-card " + this.props.className}>
        {this.props.children}
      </div>
    )
  }
}
