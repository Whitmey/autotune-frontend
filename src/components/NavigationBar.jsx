import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Logo from "./Logo";
import FeedbackDialog from "./FeedbackDialog";
import "../styles/NavigationBar.scss";

export default class Navigationbar extends Component{

  constructor(props) {
    super(props);
  }

  render() {
    return(
      <header>
        <div className="grid__row">
          <div className="grid__column">
            <Logo/>
            <nav className="left-nav">
              <ul>
                <li>
                  <Link to="/about">About</Link>
                </li>
                <li>
                  <FeedbackDialog/>
                </li>
              </ul>
            </nav>
            <nav className="right-nav">
              <ul>
                <li className="common-button info-link">
                  <Link to="/demo">Demo</Link>
                </li>
                <li className="common-button action-link">
                  <Link to="/algorithm">Run Algorithm</Link>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </header>
    );
  }

}
