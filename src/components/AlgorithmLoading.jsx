import React, { Component } from 'react';
import "../styles/AlgorithmLoading.scss";

export default class AlgorithmLoading extends Component{

  constructor(props) {
    super(props);
    this.state = {
      firstActive: false,
      secondActive: false,
      thirdActive: false
    }
  }

  componentDidMount() {
    setTimeout(function() {
      this.setState({ firstActive: true });

      setTimeout(function() {
        this.setState({ secondActive: true });

        setTimeout(function waitForElement() {
          if (this.props && this.props.response){
              this.loadingComplete();
          }
          else {
              setTimeout(waitForElement.bind(this), 250);
          }
        }.bind(this), 5000);
      }.bind(this), 5000);
    }.bind(this), 5000);
  }

  loadingComplete() {
    this.props.onLoadingComplete();
  }

  render() {
    return(
      <section className="algorithm-loading">
        <div className="loading-container">
          <div className={this.state.firstActive ? 'circle-loader load-complete' : 'circle-loader'}>
            <div className={this.state.firstActive ? 'checkmark draw show' : 'checkmark draw'}></div>
          </div>
          <span>Downloading NightScout Data</span>
        </div>
        <div className="loading-container">
          <div className={this.state.secondActive ? 'circle-loader load-complete' : 'circle-loader'}>
            <div className={this.state.secondActive ? 'checkmark draw show' : 'checkmark draw'}></div>
          </div>
          <span>Running Algorithm</span>
        </div>
        <div className="loading-container">
          <div className={this.state.thirdActive ? 'circle-loader load-complete' : 'circle-loader'}>
            <div className={this.state.thirdActive ? 'checkmark draw show' : 'checkmark draw'}></div>
          </div>
          <span>Downloading Results</span>
        </div>
      </section>
    )
  }
}
