import React, { Component } from 'react';
import NetworkService from '../services/NetworkService.js';
import MaterialCard from './MaterialCard';
import AlgorithmLoading from './AlgorithmLoading';
import AlgorithmResult from './AlgorithmResult';
import BasalProfiles from '../util/basals.js';
import Transformer from '../services/Transformer.js';
import DateRangePicker from './DateRangePicker';
import moment from 'moment';
import "../styles/AlgoForm.scss";

export default class AlgoForm extends Component{

  constructor(props) {
    super(props);
    this.NetworkService = new NetworkService();
    this.Transformer = new Transformer();
    this.state = this.props.demoData || {
      displayInputForm: true,
      displayLoadingScreen: false,
      displayResult: false,
    	url: "",
    	startDate: null,
    	endDate: null,
      isf: 0,
      carbRatio: 0,
      basals: BasalProfiles,
      algorithmResult: null
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleBasalChange = this.handleBasalChange.bind(this);
    this.handleLoadingComplete = this.handleLoadingComplete.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const name = event.target.name;
    this.setState({
      [name]: event.target.value
    });
  }

  handleBasalChange(event) {
    const index = event.target.name;
    let basals = Object.assign([], this.state.basals);
    basals[index].rate = parseFloat(event.target.value);
    this.setState({
      basals: basals
    });
  }

  handleLoadingComplete() {
    this.setState({
      displayLoadingScreen: false,
      displayResult: true
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({
      displayInputForm: false,
      displayLoadingScreen: true
    });
    this.NetworkService.runAlgorithm(this.Transformer.transformAlgoForm(this.state)).then((response) => {
      this.setState({
        algorithmResult: response
      });
    });
  }

  render() {
    return (
      <div className="container">
        {this.state.displayInputForm &&
          <MaterialCard>
            <form onSubmit={this.handleSubmit} className="algo-form">
              <div>
              </div>
              <div className="fields">
                <div className="left-container">
                  <label>
                    NightScout URL
                    <input type="text" name="url" value={this.state.url} onChange={this.handleChange} readOnly={this.props.readOnly} />
                  </label>
                  <label>
                    Insulin Sensitivity Factor (ISF)
                    <input type="number" name="isf" value={this.state.isf} onChange={this.handleChange} readOnly={this.props.readOnly} />
                  </label>
                  <label>
                    Carb Ratio (CR)
                    <input type="number" name="carbRatio" value={this.state.carbRatio} onChange={this.handleChange} readOnly={this.props.readOnly} />
                  </label>
                </div>
                <div className="right-container">
                  <h2>Basal Rates</h2>
                  <div className="basal-rates">
                    {this.state.basals.map((basal, i) =>
                      <label key={i}>
                        {basal.start.slice(0, -3)}
                        <input type="number" name={i} value={basal.rate} onChange={this.handleBasalChange} readOnly={this.props.readOnly} />
                      </label>
                    )}
                  </div>
                </div>
              </div>
              <input type="submit" value="Submit" className="common-button" id="submit-button"/>
            </form>
          </MaterialCard>
        }
        {this.state.displayLoadingScreen &&
          <AlgorithmLoading response={this.state.algorithmResult} onLoadingComplete={this.handleLoadingComplete} />
        }
        {this.state.displayResult &&
          <AlgorithmResult response={this.state.algorithmResult}/>
        }
      </div>
    );
  }

}
