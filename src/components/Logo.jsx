import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "../styles/Logo.scss";

export default class Logo extends Component{

  constructor(props) {
    super(props);
  }

  render() {
    return(
      <Link to="/" className="logo">
        <div>
          <svg width="30" height="28">
            <g>
              <path d="M27.636 8.939l1.857 3.08a4.384 4.384 0 0 1 0 4.094l-5.632 9.646C23.13 27.14 21.72 28 20.187 28h-10.4c-1.533 0-2.943-.86-3.675-2.241l-1.944-3.29 7.673-7.119 5.559 3.223c.581.398 1.36.292 1.82-.248l8.416-9.386zm-16.738.785l-8.41 9.464-1.98-3.032a4.369 4.369 0 0 1 0-4.088l5.94-9.83A4.172 4.172 0 0 1 10.125 0h10.01c1.535 0 2.946.859 3.678 2.238l2.02 3.402-7.521 6.99-5.54-3.217a1.373 1.373 0 0 0-1.874.31z" fill="#5386FF"/>
            </g>
          </svg>
          <h1 className="title">AutoTune</h1>
        </div>
      </Link>
    )
  }
}
