import React, { Component } from 'react';
import "../styles/BackgroundSecondary.scss"

export default class BackgroundSecondary extends Component{

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <svg className="background-secondary" width="662" height="620" viewBox="0 0 662 620" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clipPath="url(#clip0)">
          <path fillRule="evenodd" clipRule="evenodd" d="M660.151 619.602C397.745 604.487 525.094 266.014 303.136 332.018C81.1776 398.022 -136.31 71.9181 108.504 -122.895C353.319 -317.708 73.9752 -333.334 252.652 -529.792C431.329 -726.25 743.475 -654.334 962.475 -383.834C1181.48 -113.334 1096.42 614.936 660.151 619.602Z" fill="#F2F2FF"></path>
        </g>
        <defs>
          <clipPath id="clip0">
            <rect width="1079" height="1262" fill="white" transform="translate(0 -642)"></rect>
          </clipPath>
        </defs>
      </svg>
    )
  }
}
