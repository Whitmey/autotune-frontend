import React, { Component } from 'react';
import "../styles/Footer.scss";

export default class Footer extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return(
      <footer>
        <div className="note">
          Made with <span className="heart"> ❤ </span> by Will Whitmey
        </div>
      </footer>
    )
  }

}
