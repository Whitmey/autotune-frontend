import React, { Component} from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Background from '../components/Background';
import "../styles/Home.scss";

export default class Home extends Component{

  constructor(props) {
    super(props);
  }

  render(){
    return(
      <section>
        <Background />
        <div className="main">
          <div className="summary">
            <h1>The OpenAPS Autotune Algorithm</h1>
            <p>Run the algorithm against your NightScout URL to get suggestions on changes to your insulin basal rates, insulin sensitivity factor (ISF) and carb ratios (CR).</p>
            <div className="actions">
              <span className="common-button info-link">
                <Link to="/demo">Demo</Link>
              </span>
              <span className="common-button action-link">
                <Link to="/algorithm">Run Algorithm</Link>
              </span>
            </div>
          </div>
        </div>
      </section>
    );
  }

}
