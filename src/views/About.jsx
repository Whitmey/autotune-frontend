import React, { Component} from "react";
import "../styles/About.scss"

export default class About extends Component{

  constructor(props) {
    super(props);
  }

  render(){
    return(
      <section className="about-view">
        <div className="container">
          <h1>AutoTune</h1>
          <div className="content">
            <p>
              Autotune is a DIY tool to help calculate potential adjustments to ISF, carb ratio, and basal rates.
            </p>
            <h2>The difference between autotune and autosens:</h2>
            <p>Autosensitivity/resistance mode (aka “autosens”) is an advanced feature in OpenAPS that you can enable that looks at 24 hours of data and makes adjustments to ISF and targets based on the resulting sensitivity calculations. If you have a dying pump site, or have been sick and are resistant, your ISF is likely to be calculated down by autosens and then used in OpenAPS calculations accordingly. The opposite for being more sensitive is true as well. (Here’s a blog post describing autosensitivity during sick days.)</p>
            <p>
              Autotune, by contrast, is designed to iteratively adjust basals, ISF, and carb ratio over the course of weeks. Because it makes changes more slowly than autosens, autotune ends up drawing on a larger pool of data, and is therefore able to differentiate whether and how basals and/or ISF need to be adjusted, and also whether carb ratio needs to be changed. Whereas we don’t recommend changing basals or ISF based on the output of autosens (because it’s only looking at 24h of data, and can’t tell apart the effects of basals vs. the effect of ISF), autotune is intended to be used to help guide basal, ISF, and carb ratio changes because it’s tracking trends over a large period of time. See below for how it can be used as a manual one-off calculation or in a closed loop setting, along with notes about the safety caps designed to go with it.
            </p>
          </div>
        </div>
      </section>
    );
  }

}
