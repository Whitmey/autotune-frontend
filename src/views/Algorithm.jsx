import React, { Component} from "react";
import AlgoForm from '../components/AlgoForm';
import BackgroundSecondary from '../components/BackgroundSecondary';
import "../styles/Algorithm.scss"

export default class Algorithm extends Component{

  constructor(props) {
    super(props);
  }

  render(){
    return(
      <section>
        <BackgroundSecondary />
        <div className="main algorithm-view">
          <AlgoForm />
        </div>
      </section>
    );
  }

}
