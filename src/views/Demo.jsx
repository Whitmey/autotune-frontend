import React, { Component} from "react";
import AlgoForm from '../components/AlgoForm';
import BasalProfiles from '../util/demoBasals.js';
import BackgroundSecondary from '../components/BackgroundSecondary';

const demoData = {
  displayInputForm: true,
  displayLoadingScreen: false,
  displayResult: false,
  url: "https://william-cgm.herokuapp.com",
  startDate: "2018-12-15",
  endDate: "2019-01-10",
  isf: 81,
  carbRatio: 7.0,
  basals: BasalProfiles,
  algorithmResult: null
}

export default class Demo extends Component{

  constructor(props) {
    super(props);
  }

  render(){
    return(
      <section>
        <BackgroundSecondary />
        <div className="main algorithm-view">
          <AlgoForm
            demoData={demoData}
            readOnly={true}
          />
        </div>
      </section>
    );
  }

}
