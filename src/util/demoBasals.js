export default [
  {
    start: "00:00:00",
    minutes: 0,
    rate: 0.6
  },
  {
    start: "01:00:00",
    minutes: 60,
    rate: 0.6
  },
  {
    start: "02:00:00",
    minutes: 120,
    rate: 0.85
  },
  {
    start: "03:00:00",
    minutes: 180,
    rate: 0.70
  },
  {
    start: "04:00:00",
    minutes: 240,
    rate: 0.85
  },
  {
    start: "05:00:00",
    minutes: 300,
    rate: 0.6
  },
  {
    start: "06:00:00",
    minutes: 360,
    rate: 0.6
  },
  {
    start: "07:00:00",
    minutes: 420,
    rate: 0.85
  },
  {
    start: "08:00:00",
    minutes: 480,
    rate: 0.70
  },
  {
    start: "09:00:00",
    minutes: 540,
    rate: 0.85
  },
  {
    start: "10:00:00",
    minutes: 600,
    rate: 0.6
  },
  {
    start: "11:00:00",
    minutes: 660,
    rate: 0.6
  },
  {
    start: "12:00:00",
    minutes: 720,
    rate: 0.85
  },
  {
    start: "13:00:00",
    minutes: 780,
    rate: 0.70
  },
  {
    start: "14:00:00",
    minutes: 840,
    rate: 0.85
  },
  {
    start: "15:00:00",
    minutes: 900,
    rate: 0.6
  },
  {
    start: "16:00:00",
    minutes: 960,
    rate: 0.85
  },
  {
    start: "17:00:00",
    minutes: 1020,
    rate: 0.70
  },
  {
    start: "18:00:00",
    minutes: 2040,
    rate: 0.85
  },
  {
    start: "19:00:00",
    minutes: 2100,
    rate: 0.6
  },
  {
    start: "20:00:00",
    minutes: 2160,
    rate: 0.6
  },
  {
    start: "21:00:00",
    minutes: 2220,
    rate: 0.85
  },
  {
    start: "22:00:00",
    minutes: 2280,
    rate: 0.70
  },
  {
    start: "23:00:00",
    minutes: 2340,
    rate: 0.70
  }
]
