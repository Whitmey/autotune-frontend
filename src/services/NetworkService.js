import axios from 'axios';

export default class NetworkService {

  runAlgorithm(data) {
    return axios.post('https://autotune-api.herokuapp.com/algorithm/run', data)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  submitFeedback(data) {
    return axios.post('https://autotune-api.herokuapp.com/feedback', data)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        console.log(error);
      });
  }

}
