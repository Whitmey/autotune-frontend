export default class Transformer {

  transformAlgoForm(data) {
    return {
      "url": data.url,
      "startDate": data.startDate,
      "endDate": data.endDate,
      "currentSettings": {
        "dia": 5,
        "isf": data.isf,
        "carbRatio": data.carbRatio,
        "basalProfiles": data.basals
      }
    }
  }

}
