import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { hot } from "react-hot-loader";
import Home from "./views/Home";
import Algorithm from "./views/Algorithm";
import Demo from "./views/Demo";
import Feedback from "./views/Feedback";
import About from "./views/About";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";
import "./styles/App.scss";

export class App extends Component{

  constructor(props) {
    super(props);
  }

  render() {
    return(
      <Router>
        <div className="app">

          <NavigationBar />

          <Route exact path="/" component={Home} />
          <Route path="/algorithm" component={Algorithm} />
          <Route path="/demo" component={Demo} />
          <Route path="/feedback" component={Feedback} />
          <Route path="/about" component={About} />

          <Footer />

        </div>
      </Router>
    );
  }

}

export default hot(module)(App);
