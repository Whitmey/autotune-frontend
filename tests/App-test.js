import chai from 'chai'
import React from 'react'
import { App } from '../src/App'
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
let expect = chai.expect;

describe("spec app tests", () => {

    it('renders one div', ()=>{
        const wrapper = shallow(<App/>);
        expect(wrapper.find('div')).to.have.length(1);
    });
})
